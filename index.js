const adapterModule = require('./api/Adapters/AdapterModule');
const repositoriesModule = require('./api/Repositories/RepositoryModule');
const Enviroment = require('./enviroments/Enviroment');

const mongo = new adapterModule.data.Mongo (
    Enviroment.getMongoUrl(),
    Enviroment.getMongoDbName()
);

const mongoConected = mongo.conect();


mongoConected.then(() => {
    const dataRepository = new repositoriesModule.DataRepository(mongo);
    const express = new adapterModule.server.Express(dataRepository);
    express.initialize(8080)
})
