const MongoClient = require('mongodb').MongoClient;
const mongo = require('mongodb');

class Mongo{
    constructor(url,databaseName){
        this.url = url;
        this.databaseName = databaseName;
    }

    async conect(){
        this.conexion =  await MongoClient.connect(this.url);
        this.database = await this.conexion.db(this.databaseName);
        console.log('Sucessful conection with MongoDB');
    }

    disconect(){
        this.conexion.close();
    }

    getCollection(collectionName){
        return this.database.collection(collectionName)
    }

    async getList(collectionName){
        let collection = await this.getCollection(collectionName);
        let array = await collection.find({}).toArray();

        return array;
    }

    createSearchCriteriaById(id) {
        let o_id = new mongo.ObjectID(id); 
        return { '_id': o_id };
    }

    async getOne(id, collectionName) {
        const searchCriteria = this.createSearchCriteriaById(id);
        let collection = await this.getCollection(collectionName);
        return await collection.findOne(searchCriteria)
    }

    async deleteOne(id, collectionName) {
        const searchCriteria = this.createSearchCriteriaById(id);
        let collection = await this.getCollection(collectionName);
        return await collection.deleteOne(searchCriteria);
    }

    async updateOne(id, object, collectionName) {
        const searchCriteria = this.createSearchCriteriaById(id);
        let collection = await this.getCollection(collectionName);
        return await collection.updateOne(
            searchCriteria,
            { $set: object},
        );
    }

    async addObject(object,collectionName){
        let collection = await this.getCollection(collectionName);
        return collection.insertOne(object);
    }

    async getListWhere(paramKey, paramValue, collectionName){
        let query = {};
        query[paramKey] = paramValue;
        let collection = await this.getCollection(collectionName);
        return  await collection.find(query).toArray();
    }

    async geoIntersect(latitude,longitude,collectionName){ 
        let query = {
            location: { 
                $geoIntersects:{
                    $geometry:{
                        type: "Point",
                        coordinates: [
                            longitude,
                            latitude
                        ]
                    }
                }
            }
        };
        let collection = await this.getCollection(collectionName);
        return await collection.find(query).toArray();
    }

}

module.exports = Mongo;
