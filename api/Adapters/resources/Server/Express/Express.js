const  express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const helmet = require('helmet');
 

const RouterModule = require('./Routers/RouterModule');

class Express{

    constructor(dataRepository) {
        this.app = express();    
        this.databaseRepository = dataRepository; 
    }

    initialize(port) {
        this.configApp();
        this.defineRoutes();
        if (port) {
            this.listeToPort(port);
        }
    }

    configApp() {
        this.configCors();
        this.configBodyParser();
        this.defineRoutes();
        this.configHelment();
    }

    configHelment() {
        this.app.use(helmet());
    }

    configCors() {
        const allowedOrigins = [
            'http://localhost:4200'
        ];
        this.app.use(
            cors({
                origin: function(origin, callback){
                  
                    if(!origin) return callback(null, true);
                    if(allowedOrigins.indexOf(origin) === -1){
                      const msg = 'The CORS policy for this site does not ' +
                                'allow access from the specified Origin.';
                      return callback(new Error(msg), false);
                    }
                    return callback(null, true);
                  }
            })
        );
    }

    configBodyParser() {
        this.app.use(bodyParser.text());
        this.app.use(bodyParser.json());
    }

    defineRoutes() {
        const mainRouter = new RouterModule.MainRouter();
        this.app.use('/', mainRouter.router);
    }

    listeToPort(port) {
        this.app.listen(port,() => { 
            console.log("Express is listening to port: " + port);
        })
    }
}
         
module.exports = Express;
