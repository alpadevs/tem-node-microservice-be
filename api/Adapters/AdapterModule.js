const Express = require('./resources/Server/Express/Express');
const Mongo = require('./resources/Database/Mongo');

const adapterModule = {
    server: {
        Express
    },
    data: {
        Mongo
    }
}

module.exports = adapterModule;