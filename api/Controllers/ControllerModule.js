
const MainController = require('./resources/MainController');

const controllerModule = {
    MainController,
}

module.exports = controllerModule;