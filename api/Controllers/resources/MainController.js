class MainController{
    constructor(dataRepository){
        this.dataRepository = dataRepository; 
    }

    root(request,response){
        response.send('The Api Works');
    }
}

module.exports = MainController;