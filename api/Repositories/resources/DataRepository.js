class DataRepository {

    constructor(db){
        this.db = db;
    }

    async addObject(object, collectionName) {
       return await this.db.addObject(object,collectionName);
    }

    async getList(collectiName) {
        let list = await this.db.getList(collectiName);
        return list;
    }

    async getOne(id, collectiName) {
        let document = await this.db.getOne(id,collectiName);
        return document;
    }

    async getListWhere(paramKey, paramValue, collectionName) {
        return await this.db.getListWhere(paramKey,paramValue,collectionName);
    }

    async geoIntersect(latitude, longitude, collectionName) {
        return await this.db.geoIntersect(latitude,longitude,collectionName);
    }

    async deleteObject(id, collectionName) {
        return await this.db.deleteOne(id, collectionName);
    }

    async updateObject(id, newDocument, collectionName) {
        return await this.db.updateOne(id, newDocument, collectionName);
    }

}

module.exports = DataRepository;