const DataRepository = require('./resources/DataRepository');

const controllerModule = {
    DataRepository,
}

module.exports = controllerModule;